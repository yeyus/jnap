package jnap

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// JNAP main JNAP construct
type JNAP struct {
	endpoint   string
	user       string
	password   string
	httpClient *http.Client
}

// NewJNAP instantiates a new JNAP object
func NewJNAP(endpoint string, user string, password string) JNAP {
	return JNAP{endpoint: endpoint, user: user, password: password, httpClient: &http.Client{}}
}

// GetDevices gets all devices know by the device
func (j *JNAP) GetDevices() ([]Device, int64, error) {
	var envelope ResponseEnvelope
	var devicesResponse GetDeviceResponse

	jsonStr := []byte(`[{"action":"http://linksys.com/jnap/devicelist/GetDevices","request":{"sinceRevision":0}}]`)
	resp, err := j.request(jsonStr)
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	err = json.Unmarshal(body, &envelope)
	err = json.Unmarshal(envelope.Responses[0].Output, &devicesResponse)

	return devicesResponse.Devices, devicesResponse.Revision, err
}

// request internal utility method to perform http requests
func (j *JNAP) request(reqBody []byte) (*http.Response, error) {
	req, _ := http.NewRequest("POST", j.endpoint, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	req.Header.Set("X-JNAP-Action", "http://linksys.com/jnap/core/Transaction")
	req.SetBasicAuth(j.user, j.password)

	resp, err := j.httpClient.Do(req)

	return resp, err
}

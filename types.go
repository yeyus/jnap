package jnap

import "encoding/json"

// ResponseEnvelope an envelope for multiple JNAP responses
type ResponseEnvelope struct {
	Result    string     `json:"result"`
	Responses []Response `json:"responses"`
}

// Response envelope for single JNAP response
type Response struct {
	Result string          `json:"result"`
	Output json.RawMessage `json:"output"`
}

// GetDeviceResponse response for JNAP GetDevices call
type GetDeviceResponse struct {
	Revision int64    `json:"revision"`
	Devices  []Device `json:"devices"`
}

// Device data structure for a device known by the router
type Device struct {
	DeviceID             string             `json:"deviceID"`
	LastChangeRevision   int64              `json:"LastChangeRevision"`
	Model                DeviceModel        `json:"model"`
	Unit                 DeviceUnit         `json:"unit"`
	IsAuthority          bool               `json:"IsAuthority"`
	FriendlyName         string             `json:"friendlyName"`
	KnownMACAddresses    []string           `json:"knownMACAddresses"`
	Connections          []DeviceConnection `json:"connections"`
	Properties           []DeviceProperty   `json:"properties"`
	MaxAllowedProperties int64              `json:"maxAllowedProperties"`
}

// DeviceModel data structure identifying a device model
type DeviceModel struct {
	DeviceType   string `json:"deviceType"`
	Manufacturer string `json:"manufacturer"`
	ModelNumber  string `json:"modelNumber"`
}

// DeviceUnit data structure identifying a device unit
type DeviceUnit struct {
	OperatingSystem string `json:"operatingSystem"`
}

// DeviceConnection data structure for connected devices
type DeviceConnection struct {
	MacAddress string `json:"macAddress"`
	IPAddress  string `json:"ipAddress"`
}

// DeviceProperty key-value extra information for Device
type DeviceProperty struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}
